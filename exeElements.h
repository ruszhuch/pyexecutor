#ifndef EXE_ELEMENTS_H
#define EXE_ELEMENTS_H

#include "PECommon.h"
#include "exeBasic.h"

/////////////////////////////////////////////////////////
//**module exceptions
/////////////////////////////////////////////////////////
class ModuleLoadFailedException : public std::runtime_error
{
public:

	ModuleLoadFailedException::ModuleLoadFailedException()
		: runtime_error("Module exception: failed to load module") {}

};

class ModuleObjectLoadFailedException : public std::runtime_error
{
public:

	ModuleObjectLoadFailedException::ModuleObjectLoadFailedException()
		: runtime_error("Module exception: failed to load object") {}

};

/////////////////////////////////////////////////////////
//**module
/////////////////////////////////////////////////////////
//����� ��������� ����������� ������ 
//��������� �������� ���������� ������������ ������ (import modulename), 
//������, ������ ������� (�������, �����, �����, ...) ���������� ��������� �������� 
class Module : public exeBasic
{

public:

	Module();
	~Module();

	//��������� ������ �� ����� ����� (��� .py), ��������: pygletTest
	virtual void load(PyObject* moduleName) override;

	//��������� ����������� ������ �� ����� 
	virtual void loadObject(std::string& objectName);
	//����������� ����������� ������ �� ����� 
	virtual void releaseObject(std::string& objectName);

	//���������� ����������� ������ �� ����� 
	virtual exeBasic* getObject(std::string& objectName);

private:

private:

protected:

	//������ ����������� ������� 
	std::map <std::string, exeBasic*> loadedObjects;


};

/////////////////////////////////////////////////////////
//**object exception
/////////////////////////////////////////////////////////
class ObjectException : public std::exception
{

public:

	virtual const char* what() const throw() = 0;

};

class ObjectLoadFailedException : public ObjectException
{

public:

	virtual const char* what() const throw()
	{
		return "Object exception: Failed to load";
	}

};

class ObjectFatherFailedException : public ObjectException
{

public:

	virtual const char* what() const throw()
	{
		return "Object exception: Failed to find father object";
	}

};

/////////////////////////////////////////////////////////
//**object
/////////////////////////////////////////////////////////
//��������������� ��������� ����������� �����, ������� ��� �����
//������ ��������� ��� �� ��������� �������, ��� � �� ������-�������� 
class Object : public Module
{

public: 
	Object();
	~Object();

	//��������� ����� 
	virtual void load(PyObject* fatherName, PyObject* objectName);

	//�������� ������� ��� �����
	virtual PyObject* exe(double* arg, unsigned __int8 numOfArgs, bool asMethod = false);

protected:

	PyObject* father;

};

#endif // !EXE_ELEMENTS_H