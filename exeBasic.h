#ifndef EXE_BASIC_H
#define EXE_BASIC_H

#include "PECommon.h"

class ExecutorModulePythonRuntimeException : public std::runtime_error
{
public:
	ExecutorModulePythonRuntimeException::ExecutorModulePythonRuntimeException()
		: runtime_error("Executor exception: Python runtime error") {
		PyErr_Print();
	}
};

//����������� �����, �������� PyObject
class exeBasic
{

public:

	exeBasic() {}
	~exeBasic() {}

	virtual void load(PyObject* name) = 0;

protected:

	//��� �������
	PyObject* name;
	//��������� �� ����������� ������  
	PyObject* me;

};

namespace AMAP
{

	static exeBasic* findInMap(std::string& key, std::map<std::string, exeBasic*>& m)
	{

		if (!key.compare(""))
			return nullptr;

		auto& It = m.find(std::string(key));
		if (It != std::end(m))
		{
			return It->second;
		}

		return nullptr;

	}

	static void deleteFromMap(std::string& key, std::map<std::string, exeBasic*>& m)
	{

		if (!key.compare(""))
			return;

		auto& It = m.find(std::string(key));
		if (It != std::end(m))
		{
			if (It->second)
				Py_DECREF(It->second);

		}

		return;

	}

}


#endif // !EXE_BASIC_H