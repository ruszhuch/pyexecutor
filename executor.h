#ifndef EXECUTOR_H
#define EXECUTOR_H

#include "PECommon.h"

#include "exeElements.h"




/////////////////////////////////////////////////////////
//**executor exceptions
/////////////////////////////////////////////////////////
class ExecutorException : public std::runtime_error
{

public:

	ExecutorException::ExecutorException()
		: runtime_error("Executor exception") {}

};

class ExecutorDirLoadException : public std::runtime_error
{
public: 
	ExecutorDirLoadException::ExecutorDirLoadException()
		: runtime_error("Executor exception: can't find directory to load") {}
};

class ExecutorObjectLoadException : public std::runtime_error
{
public:
	ExecutorObjectLoadException::ExecutorObjectLoadException()
		: runtime_error("Executor exception: Failed to load object") {}

};

class ExecutorModuleLoadException : public std::runtime_error
{
public:
	ExecutorModuleLoadException::ExecutorModuleLoadException()
		: runtime_error("Executor exception: Failed to load module") {}

};

class ExecutorModuleFindException : public std::runtime_error
{
public:
	ExecutorModuleFindException::ExecutorModuleFindException()
		: runtime_error("Executor exception: Failed to find module") {}

};

/////////////////////////////////////////////////////////
//**executor
/////////////////////////////////////////////////////////
//�������� ������� ��� ���������� ������������ ���������� 
//�� ������ ����� ����������� ������ � �������� 
class Executor
{

public:

	PELINK void init();
	PELINK void release();

	//���������� ������ sys � ��������� ���� � ������ �������� �� ������ 
	PELINK void setup(std::vector<std::string>& moduleDirs);

	//��������� ������ 
	PELINK void importModule(std::string& moduleName);

	//����������� ������ 
	PELINK void releaseModule(std::string& moduleName);

	//���������� ��������� �� ������ 
	PELINK Module* getModule(std::string& moduleName);

private:

private:

	//������ ����������� ������ 
	std::map <std::string, exeBasic*> loadedModules;

};

PELINK Executor* initExecutor();
PELINK void releaseExecutor(Executor* ptr);

#endif // !EXECUTOR