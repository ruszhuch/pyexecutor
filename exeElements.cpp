#include "exeElements.h"

/////////////////////////////////////////////////////////
//**module
/////////////////////////////////////////////////////////
Module::Module()
	: exeBasic()
{

}

Module::~Module()
{
	Py_DECREF(this->name);
	Py_DECREF(this->me);
}

void Module::load(PyObject* moduleName)
{

	//�������� ��� � ��������� �� ������� 
	this->name = moduleName;
	this->me = PyImport_Import(this->name);

	if (PyErr_Occurred())
	{
		PyErr_Print();
		//TODO: Throw exception
	}
	if (!this->me)
	{
		PE_PRINT("Failed to load module: " << PyUnicode_AsUTF8(this->name));
		throw ModuleLoadFailedException();
		return;
	}

}

void Module::loadObject(std::string& objectName)
{

	//������ �� ��������� �������� ������� 
	if (AMAP::findInMap(objectName, this->loadedObjects))
	{
		PE_PRINT("Object '" << objectName << "' has been already loaded");
		return;
	}

	//��������� ������
	Object* NewObject = new Object();

	try
	{
		NewObject->load(this->me, PyUnicode_FromString(objectName.c_str()));
	}
	catch (const ObjectLoadFailedException& e)
	{
		throw ModuleObjectLoadFailedException();
		return;
	}
	catch (const ObjectFatherFailedException& e)
	{
		throw ModuleObjectLoadFailedException();
		return;
	}

	//��������� ����������� ������ � ������
	this->loadedObjects.insert(std::pair<std::string, exeBasic*>(objectName, NewObject));

	PE_PRINT("Object '" << objectName << "' has been loaded");

}

void Module::releaseObject(std::string& objectName)
{

	//��������� ������, ���� �� ����� ��� ��������
	PyObject* object = (PyObject*)AMAP::findInMap(objectName, this->loadedObjects);

	if (!object)
	{
		PE_PRINT("Can't delete object '" << objectName << "'. Object does not exist");
		return;
	}

	AMAP::deleteFromMap(objectName, this->loadedObjects);

	PE_PRINT("Object '" << objectName << "' has been released");

}

exeBasic* Module::getObject(std::string& objectName)
{

	Object* object = dynamic_cast<Object*>(AMAP::findInMap(objectName, this->loadedObjects));

	if (!object)
	{
		PE_PRINT("Can't find object: " << objectName);
		return nullptr;
	}

	return object;

}

/////////////////////////////////////////////////////////
//**object
/////////////////////////////////////////////////////////
Object::Object()
	:Module()
{



}

Object::~Object()
{



}

void Object::load(PyObject* father, PyObject* objectName)
{

	this->name = objectName;
	//�������� ��������� �� ������, ������ �������� ������������� ������ ������ 
	//(��� ������ ��� ����� �����, ��� ������ - ��������, ������)
	this->father = father;

	if (!this->father)
	{
		PE_PRINT("Failed to load object '" << PyUnicode_AsUTF8(objectName) << "': father object does't exist");
		throw new ObjectFatherFailedException;
		return;
	}

	//�������� ������ 
	this->me = PyObject_GetAttr(this->father, this->name);
	if (PyErr_Occurred())
	{
		throw ExecutorModulePythonRuntimeException();
		return;
	}
	if (!this->me)
	{
		PE_PRINT("Failed to load object: " << PyUnicode_AsUTF8(this->name));
		throw ObjectLoadFailedException();
		return;
	}

}

PyObject* Object::exe(double* arg, unsigned __int8 numOfArgs, bool asMethod)
{

	PyObject* args = nullptr;
	
	//������ ������ ��� ���������� 
	args = PyTuple_New(numOfArgs+1);

	//���� ����� ��������� �� ������, ������� ������ ����� 
	if (asMethod)
		PyTuple_SetItem(args, 0, this->father);

	//��������� ��������� 
	for (unsigned __int8 i = 0; i < numOfArgs; i++)
	{
		PyTuple_SetItem(args, asMethod ? (i + 1) : i, PyFloat_FromDouble(arg[i]));
	}

	//�������� ������� ��� �����
	//TODO: ���������� ���������� 
	PyObject* result = PyObject_CallObject(this->me, args);

	Py_DECREF(args);

	return result;

}