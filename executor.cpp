#include "executor.h"


/////////////////////////////////////////////////////////
//**Executor
/////////////////////////////////////////////////////////
Executor* initExecutor()
{

	Executor* newExecutor = new Executor();
	newExecutor->init();

	return newExecutor;

}

void releaseExecutor(Executor* ptr)
{

	if (!ptr)
		return;

	ptr->release();

	delete ptr;

}

void Executor::init()
{

	Py_Initialize();

	PE_PRINT("Python executor has been created");

}

void Executor::release()
{

	//TODO: Release all memory

	Py_Finalize();

	PE_PRINT("Python executor has been released");

}

void Executor::setup(std::vector<std::string>& moduleDirs)
{

	//��������� ������ sys
	PyRun_SimpleString("import sys");
	PyRun_SimpleString("sys.argv = ['']");
	//��������� ���� � ������� 
	std::string str;
	for (uint8_t i = 0; i < moduleDirs.size(); i++)
	{
		str = "sys.path.append('" + moduleDirs.at(i) + "')";
		PyRun_SimpleString(str.c_str());
		if (PyErr_Occurred())
		{
			throw ExecutorModulePythonRuntimeException();
			return;
		}
	}
	
	PE_PRINT("Python executor setup complete");

}

void Executor::importModule(std::string& moduleName)
{

	moduleName = moduleName.substr(0, moduleName.size() - 3);

	//������ �� ��������� �������� ������ 
	if (AMAP::findInMap(moduleName, this->loadedModules))
	{
		PE_PRINT("Module '" << moduleName << "' has been already loaded");
		return;
	}
		
	//��������� ������ 
	Module* newModule = new Module();

	try
	{
		newModule->load(PyUnicode_FromString(moduleName.c_str()));
	}
	catch (const ModuleLoadFailedException& e)
	{
		throw ExecutorModuleLoadException();
		return;
	}

	this->loadedModules.insert(std::pair<std::string, exeBasic*>(moduleName, newModule));

	PE_PRINT("Module '" << moduleName << "' has been loaded");

}

void Executor::releaseModule(std::string& moduleName)
{

	//��������� ������, ���� �� ��� �������� 
	PyObject* module = (PyObject*)AMAP::findInMap(moduleName, this->loadedModules);

	if (!module)
	{
		PE_PRINT("Can't delete module '" << moduleName << "'. Module does not exist");
		return;
	}

	AMAP::deleteFromMap(moduleName, this->loadedModules);

	PE_PRINT("Module '" << moduleName << "' has been released");

}

Module* Executor::getModule(std::string & moduleName)
{

	Module* module = dynamic_cast<Module*>(AMAP::findInMap(moduleName, this->loadedModules));

	if (!module)
	{
		PE_PRINT("Can't find module: " << moduleName);
		throw ExecutorModuleFindException();
		return nullptr;
	}

	return module;

}