#ifndef PE_COMMON_H
#define PE_COMMON_H

#include "PELinker.h"

#include <Windows.h>

#include <iostream>

#include <string>
#include <vector>
#include <map>

#include <exception>

#ifdef _DEBUG
#undef _DEBUG
#include <python.h>
#define _DEBUG
#else
#include <python.h>
#endif

#define PE_DEBUG 

//PE_DEBUG
#ifdef PE_DEBUG

#define PE_PRINT(text) {std::cout << text << std::endl;}

#else // PE_DEBUG END

#define PE_PRINT(text) {}

#endif // !PE_DEBUG

#endif // !PE_COMMON_H